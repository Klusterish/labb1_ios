//
//  ViewController.h
//  Lindvin
//
//  Created by Kluster on 2016-01-31.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *textColorSwitch;

@end

